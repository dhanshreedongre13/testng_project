package API_common_methods;

import static io.restassured.RestAssured.given;

public class Common_Put_Method_ {

	public static int put_statusCode(String requestBody, String endpoint) {

		int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().statusCode();
		return statusCode;
	}

	public static String put_responseBody(String requestBody, String endpoint) {

		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().response().asString();
		return responseBody;

	}
}