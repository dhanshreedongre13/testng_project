package TestCase_package;

import java.io.File;

import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import endpoint.Post_endpoint;
import io.restassured.path.json.JsonPath;
import request_repository.Post_request_repository;

public class Post_TC1 extends Common_method_handle_API {

	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;

	@BeforeTest
	public static void Test_Setup() throws IOException {
		log_dir = Handle_directory.create_log_directory("Post_TC1_logs");
		requestBody = Post_request_repository.post_request_tc1();
		endpoint = Post_endpoint.post_endpoint_tc1();
	}

	@Test(description="Validate responseBody")

	public static void post_executor() throws IOException {

		for (int i = 0; i < 5; i++) {

			int statusCode = post_statusCode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 201) {

				responseBody = post_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				Post_TC1.validator(requestBody, responseBody);
				break;
			} else {
				System.out.println("expected statuscode is not found hence retrying");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_id = jsp_res.getString("id");
		String res_job = jsp_res.getString("job");
		String res_createdate = jsp_res.getString("createdAt");
		res_createdate = res_createdate.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdate, expecteddate);
	}

	@AfterTest
	public static void Test_Teardown() throws IOException {
		String testclassname = Post_TC1.class.getName();
		Handle_api_logs.evidence_creator(log_dir, testclassname, endpoint, requestBody, responseBody);

	}
}
