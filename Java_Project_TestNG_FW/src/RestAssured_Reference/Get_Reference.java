package RestAssured_Reference;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class Get_Reference {

	public static void main(String[] args) {
//Step 1 :- Declare Base URL
		RestAssured.baseURI = "https://reqres.in/";

//Step2 :- Declare The expected result
		int id[] = { 1, 2, 3, 4, 5, 6 };
		String email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };
		String first_name[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
		String last_name[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };

//Step 3 :- Configure the requestbody parameter and trigger the API
		String responsebody = given().when().get("api/users?page=1").then().extract().response().asString();
		System.out.println("responsebody is :" + responsebody);

		JSONObject array_res = new JSONObject(responsebody);
		JSONArray dataarray = array_res.getJSONArray("data");
		System.out.println(dataarray);
		int count = dataarray.length();
		System.out.println(count);

		for (int i = 0; i < count; i++) {
			int exp_id = id[i];
			String exp_email = email[i];
			String exp_first_name = first_name[i];
			String exp_last_name = last_name[i];
			
//step4 :- validate responsebody
			int res_id = dataarray.getJSONObject(i).getInt("id");
			String res_email = dataarray.getJSONObject(i).getString("email");
			String res_first_name = dataarray.getJSONObject(i).getString("first_name");
			String res_last_name = dataarray.getJSONObject(i).getString("last_name");

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_email, exp_email);
			Assert.assertEquals(res_first_name, exp_first_name);
			Assert.assertEquals(res_last_name, exp_last_name);
		}

	}
}