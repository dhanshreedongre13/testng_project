package RestAssured_Reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;
import java.time.LocalDateTime;
import org.testng.Assert;

public class put_Reference {
	public static void main(String[] args) {
//Step 1:- Declare Base URL
		RestAssured.baseURI = "https://reqres.in/";

//Step 2:- Configure the request parameter and trigger the API.
		String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		String responsebody = given().header("Content-Type", "application/json")
				.body("{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}").when()
				.put("api/users/2").then().extract().response().asString();
		System.out.println("Responsebody is:" + responsebody);

//Step 3:- Create an object of json path to parse the requestbody and then the responsebody.
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime Currentdate = LocalDateTime.now();
		String expecteddate = Currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responsebody);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_date = jsp_res.getString("updatedAt");
		res_date = res_date.substring(0, 11);

//Step4 :- Validate Responsebody
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, expecteddate);

	}

}