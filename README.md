###################   TestNG_Framework_Project  ################

** TestNG_Project ** :

TestNG is an open-source test automation framework for Java. It is developed on the same lines as JUnit and NUnit. A few advanced and useful features provided by TestNG make it a more robust framework than its peers. The NG in TestNG stands for "Next Generation" using multiple annotations, grouping, dependence, prioritization, and parametrization features.

** TestNG_File_Parameterization **:
Parameterization is a powerful feature of TestNG that allows you to write more efficient and effective tests by reusing the same test logic with different input data.. It is useful when you need to test a particular functionality with different input values to ensure that it works correctly in all scenarios.

** Test Script ** : 
In this we have all the available test scripts those needs to be created.

** Data/Variable File ** :
 We are using the excel files to input the data to our framework or requestBody parameters to input from excel files.
** API related common functions ** : 
Which are used to trigget the API, extract the responseBody & extract the response status code.

** Utilities Common Methods ** :
 We have class to create the directory, if it doesn't exist in the project for test log files and if it does exist than to delete the old one and create the new one for log files. Also we have another utility from which all the endpoint, requestBody, responseBody can be stored in the notepad/text file. And another utility to read the value/data from an excel file.

** Libraries ** : 
We are using RestAssured, Apache-Poi,allure
-testNG, testng libraries and dependency management of libraries are being managed by maven repository.

** Reports ** :
 We are using Allure reports and Extent reports.
